resource "aws_acm_certificate" "diplom_cert" {
  domain_name       = "steydev.online"
  validation_method = "DNS"
}

resource "aws_route53_record" "dns_validation" {
  for_each = {
    for dvo in aws_acm_certificate.diplom_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = "Z0222224I41U0E5XA8Q8"
}

resource "aws_acm_certificate_validation" "cert_validation" {
  certificate_arn         = aws_acm_certificate.diplom_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.dns_validation : record.fqdn]
}

resource "aws_route53_record" "load_balancer" {
  zone_id = "Z0222224I41U0E5XA8Q8"
  name    = "steydev.online"
  type    = "A"
  alias {
    name = module.alb.lb_dns_name
    zone_id = module.alb.lb_zone_id
    evaluate_target_health = true
  }
}

