resource "aws_instance" "AppServer1" {
  ami = "ami-0245697ee3e07e755"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.DiplomPublic1.id
  associate_public_ip_address = true
  key_name = aws_key_pair.lesson44.key_name

  vpc_security_group_ids = [ aws_security_group.DiplomSG.id ]

  root_block_device {
    delete_on_termination = true
    volume_type = "gp2"
    volume_size = 8
  }
  
  provisioner "remote-exec" {
      connection {
        user = "admin"
        type = "ssh"
        private_key = "${file(var.ssh_key_private)}"
        timeout = "2m"
        host = aws_instance.AppServer1.public_ip
 }
    inline = [
        "sudo apt update",
        "sudo apt install -y nginx",
        "sudo systemctl restart nginx"
    ]
  } 
  
  tags = {
    "Name" = "Server1"
  }
}

resource "aws_instance" "AppServer2" {
  ami = "ami-0245697ee3e07e755"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.DiplomPublic2.id
  associate_public_ip_address = true
  key_name = aws_key_pair.lesson44.key_name

  vpc_security_group_ids = [ aws_security_group.DiplomSG.id ]

  root_block_device {
    delete_on_termination = true
    volume_type = "gp2"
    volume_size = 8
  }
  
  provisioner "remote-exec" {
      connection {
        user = "admin"
        type = "ssh"
        private_key = "${file(var.ssh_key_private)}"
        timeout = "2m"
        host = aws_instance.AppServer2.public_ip
 }
    inline = [
        "sudo apt update",
        "sudo apt install -y nginx",
        "sudo systemctl restart nginx"
    ]
  }

  tags = {
    "Name" = "Server2"
  }
}

output "publicip1" {
  value = aws_instance.AppServer1.public_ip
}

output "publicip2" {
  value = aws_instance.AppServer2.public_ip
}