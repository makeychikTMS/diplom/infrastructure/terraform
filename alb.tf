module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"

  name = "DiplomALB"

  load_balancer_type = "application"

  vpc_id             =  aws_vpc.DiplomVPC.id
  subnets            = [aws_subnet.DiplomPublic1.id, aws_subnet.DiplomPublic2.id]
  security_groups    = [aws_security_group.DiplomSG.id]


  target_groups = [
    {
      name_prefix      = "pref-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
      targets = [
        {
          target_id = aws_instance.AppServer1.id
          port = 80
        },
        {
          target_id = aws_instance.AppServer2.id
          port = 80
        }
      ]
    }
  ]

  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = aws_acm_certificate_validation.cert_validation.certificate_arn
    }
  ]

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

  tags = {
    Environment = "Diplom"
  }
}
