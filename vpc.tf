resource "aws_vpc" "DiplomVPC" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    "Name" = "DiplomMain"
  }
}

resource "aws_internet_gateway" "DiplomIGW" {
  vpc_id = aws_vpc.DiplomVPC.id

  tags = {
      "Name" = "Diplom"
  }
}

resource "aws_subnet" "DiplomPublic1" {
  vpc_id = aws_vpc.DiplomVPC.id
  cidr_block = "10.0.11.0/24"
  availability_zone = "eu-central-1a"
  tags = {
    "Name" = "Diplom-Public-1-A"
  }
}

resource "aws_subnet" "DiplomPublic2" {
  vpc_id = aws_vpc.DiplomVPC.id
  cidr_block = "10.0.12.0/24"
  availability_zone = "eu-central-1b"
  tags = {
    "Name" = "Diplom-Public-1-B"
  }
}

resource "aws_route_table" "diplompublicroutes" {
  vpc_id = aws_vpc.DiplomVPC.id
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.DiplomIGW.id
  }
  tags = {
    "Name" = "DiplomPublicRouteDev"
  }
}

resource "aws_route_table_association" "public1add" {
  subnet_id = aws_subnet.DiplomPublic1.id
  route_table_id = aws_route_table.diplompublicroutes.id
}

resource "aws_route_table_association" "public2add" {
  subnet_id = aws_subnet.DiplomPublic2.id
  route_table_id = aws_route_table.diplompublicroutes.id
}
